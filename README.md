**MVC Web API with Basic Crud Operations with EntityFramework and LINQ**

This MVC application help you provide basic Add,update,delete,show Operation using Web API MVC

**Technologies Invovled**
> Visual Studio 2017
> Mysql
> Entity Framework

-----------------------------------------------------------------------
**API Methods:**

To Insert Data /api/values/insertdata?name=abc&nic=123&cell=123
where: 
/api defines API 
/values define controller 
/insertdata define method 
name,nic,cell are parameters

---------------------------------------------------------------
**To Update Data** 
/api/values/updatedata?id=1&name=abc&nic=123&cell=123

where: 
/api defines API 
/values define controller 
/updatedata define method 
id defines the id of record (of which data needs to be updated) 
name,nic,cell are parameters

---------------------------------------------------------------

**To Delete Data** 
/api/values/delete?id=1

where: 
/api defines API 
/values define controller 
/delete define method 
id define id of reccord (id of record which needs to be deleted

-----------------------------------------------------------------

**To Reterive Data based on id **

/api/values/getdata?id=1

where: /api defines API 
/values define controller 
/getdata define method 
id defines to fetch specific record