﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPITask4.Models;
using System.Data;

namespace WebAPITask4.Controllers
{
    public class ValuesController : ApiController
    {
    
      

        [HttpGet]// Get Data by Id
        public string GetData(int id)
        {
            //String to Get Data
            string list = "";

            //using UserModle Entites Context
            using (var context = new UserModelEntities())
            {
                //Linq Query to Reterive Data from Context based on id
                var query = from p in context.user_info where p.id.Equals(id)
                            select p;
                
                //add user to list if there is any
                foreach(var user in query)
                {
                    list += user.id +" " +user.name +" "+user.nic + " " +user.cell + " " ;
                }

            }
            
            //return list
            return list;
            

        }


        [HttpGet]//insert data
        public void insertData(string name, string nic, string cell)
        {
            //User Model Entites
            UserModelEntities ume = new UserModelEntities();
            
                //Creating object of user to save data
                user_info info = new user_info();

                //inserting data
                info.name = name;
                info.nic = nic;
                info.cell = cell;
                
                //passing object to insert data
                ume.user_info.Add(info);
                
                //saving changes
                ume.SaveChanges();
        }


        [HttpGet]//update data
        public string UpdateData(int id,string name, string nic, string cell)
        {
            //user model entites
             UserModelEntities ume = new UserModelEntities();
            
            //objected created - and finding record based on id provided 
            user_info info = ume.user_info.FirstOrDefault(e => e.id.Equals(id));

            //if record exist agianst id, update the following fields
            info.name = name;
            info.nic = nic;
            info.cell = cell;

            //Finally, save the changes
            ume.SaveChanges();
            return "data updated";
        }
        
        [HttpGet]//delete the data
        public string Delete(int id)
        {
            //using User Entites
            using (var ume = new UserModelEntities())
            {
                //linq query to search if record exist against provided id
                var query = (from p in ume.user_info
                            where p.id.Equals(id)
                            select p).FirstOrDefault();

                //if exist, delete that record
                ume.user_info.Remove(query);

                //save changes
                ume.SaveChanges();

            }




            return "data Deleted";
        }

      
       
    }
}
